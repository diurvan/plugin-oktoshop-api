# Plugin OkToShop Api

Plugin para mostrar la información alimenticia traída desde la API de OkToShop Chile. Genera un Shortcode para ejecutar el proceso y muestra información en el Front del producto. Repositorio privado https://gitlab.com/diurvan/diu-oktoshop-api

![Imagen de Ruleta](https://gitlab.com/diurvan/plugin-oktoshop-api/-/raw/main/OkToShop_Informacion.png)

Este plugin utiliza el EAN del producto configurado en WooCommerce, y actualiza mediante un proceso en lote, la información adicional con la que cuenta la API de Ok To Shop.
Con esta información, se rellenan campos personalizados del producto, y se muestran en la web hacia el cliente.

Contribuye con tus comentarios.

Visita mi web en https://diurvanconsultores.com
